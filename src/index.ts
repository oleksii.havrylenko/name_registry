import * as ICAgent from '@dfinity/agent';
import fetch from 'node-fetch';
// import { Ed25519KeyIdentity } from '@dfinity/auth';
import { Crypto } from '@peculiar/webcrypto';
import * as fs from 'fs';
import * as path from 'path';

// From .dfx/local/canisters/name_registry
const IcpNameRegistryDid = ({ IDL }: { IDL: any }) => {
	const User = IDL.Record({
		'id': IDL.Nat64,
		'principal': IDL.Principal,
		'name': IDL.Text,
	});
	return IDL.Service({
		'getUsers': IDL.Func([], [ IDL.Vec(User) ], [ 'query' ]),
		'register': IDL.Func([ IDL.Text ], [ IDL.Nat64 ], []),
		'update': IDL.Func([ IDL.Nat64, IDL.Text ], [], [ 'oneway' ]),
		'whoami': IDL.Func([], [ IDL.Principal ], []),
	});
};


(global as any).crypto = new Crypto();

const ICP_ENDPOINT = 'http://localhost:8000';
const ICP_ED25519 = [ '302a300506032b6570032100b7871183064ab70580d5bab2035c6c6ac2c8ab9f5306a311d3335eec6d03df27', '5c7bb94074101a76513285d047e83a9dd3fd6eca86daf58f09580420842de9f3b7871183064ab70580d5bab2035c6c6ac2c8ab9f5306a311d3335eec6d03df27' ];

class CanisterAPI {
	public id: ICAgent.Principal|null = null;
	readonly httpAgent: ICAgent.HttpAgent;
	private agentCanister: any;

	constructor(public name: string) {
		this.httpAgent = new ICAgent.HttpAgent({
			// source?: HttpAgent;
			fetch: fetch,
			host: ICP_ENDPOINT,
			identity: new ICAgent.AnonymousIdentity(),
			// identity?: Identity | Promise<Identity>;
			// credentials?: {
			//     name: string;
			//     password?: string;
			// };
		});
	}

	async deploy(): Promise<ICAgent.Principal> {
		console.debug(`Deploy Canister "${this.name}"`);

		if ( this.id ) {
			throw new Error(`Canister "${this.id}" already deployed`);
		}

		// const signIdentity = Ed25519KeyIdentity.fromJSON(ICP_ED25519);

		this.id = await ICAgent.Actor.createCanister({
			agent: this.httpAgent,
		});

		try {
			await ICAgent.Actor.install(
				{
					module: ICAgent.blobFromBuffer(
						fs.readFileSync(
							path.resolve(
								__dirname,
								'../.dfx/local/canisters/name_registry/name_registry.wasm',
							),
						) as any,
					),
					mode: ICAgent.CanisterInstallMode.Install,
					arg: undefined,
					computerAllocation: 10,
					memoryAllocation: 10000000,
				},
				{
					agent: this.httpAgent,
					canisterId: this.id,
				},
			);
			console.info(`Successfully installed "${this.name}"`);
			console.debug(`New canister "${this.id}" installed`);

			this.agentCanister = ICAgent.makeActorFactory(IcpNameRegistryDid)({
				canisterId: this.id,
				agent: this.httpAgent,
			});

		} catch ( e ) {
			console.error(`Error while installing: ${e.message}`, e);
			throw e;
		}

		const whoami = await this.agentCanister.whoami();
		console.debug(`Canister whoami: ${whoami}`);

		return this.id;
	}

	async exec<TReturn>(method:string, ...args: any[]): Promise<TReturn> {
		console.debug(`Execute method "${method}"`, args);
		const results: TReturn = await this.agentCanister[method](...args);
		console.debug(`Method "${method}" results`, results);

		return results;
	}
}

(async () => {
	const api = new CanisterAPI('name_registry');
	await api.deploy();
	await api.exec<number>('register','alex');
	await api.exec<number>('register','alex2');
	await api.exec<number>('register','alex3');

	const users = await api.exec<any[]>('getUsers');

})();
