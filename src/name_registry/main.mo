import Principal "mo:base/Principal";
import Array "mo:base/Array";
import Types "types";

actor {

    type User = Types.User;

    var users: [User] = [];
    var nextId: Nat64 = 1;

    // returns the principal of the authenticated user
    public shared (msg) func whoami() : async Principal {
        return msg.caller;
    };

    // returns the user number
    public shared (msg) func register(name: Text): async Nat64 {
        var user : User = {
          id = nextId;
          name = name;
          principal = msg.caller;
        };
        users := Array.append<User>([user], users);
        nextId += 1;

        return user.id;
    };

    // update the name for the user
    public func update(id: Nat64, name: Text): () {
        users := Array.map<User, User>(users, func (user : User) : User {
            if (user.id == id) {
                return {
                    id = user.id;
                    name = name;
                    principal = user.principal;
                }
            };

            return user;
        });
    };

    // returns a tuple of User (number, name, Principal) array
    public query func getUsers(): async [User] {
        return users;
    };

};
