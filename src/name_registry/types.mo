import Principal "mo:base/Principal";

module Types {
    // Define User properties
    public type User = {
        id: Nat64;
        name: Text;
        principal: Principal;
    };
}
